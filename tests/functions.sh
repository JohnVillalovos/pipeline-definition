#!/bin/bash
set -euo pipefail

# shellcheck source-path=SCRIPTDIR/..
source tests/source_functions.sh
# shellcheck source-path=SCRIPTDIR/..
source tests/helpers.sh

_failed_init

function check_owner_repo {
    declare -A OWNER_REPOS

    OWNER_REPOS[http://git-extensions-are-stripped/a/b/c.git]=b.c
    OWNER_REPOS[http://extension-and-slashes-are-stripped/a/b/c.git/]=b.c
    OWNER_REPOS[http://slashes-are-stripped/a/b/c/]=b.c
    OWNER_REPOS[http://two-directories/a/b/c]=b.c
    OWNER_REPOS[http://one-directory/a/b]=a.b
    OWNER_REPOS[http://no-directory/a]=git.a
    OWNER_REPOS[http://UPPER-CASE/A/B/C]=b.c

    OWNER_REPOS[git://host.com/name1-1.1.a.git]=git.name1-1.1.a
    OWNER_REPOS[http://host.com/git/name1-1.1.a.git]=git.name1-1.1.a
    OWNER_REPOS[git://host.com/name2.git]=git.name2
    OWNER_REPOS[http://host.com/git/name2.git]=git.name2
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/arm64/linux.git]=arm64.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/davem/net-next.git]=davem.net-next
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/jejb/scsi.git]=jejb.scsi
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git]=next.linux-next
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/powerpc/linux.git]=powerpc.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/rdma/rdma.git]=rdma.rdma
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-rt-devel.git]=rt.linux-rt-devel
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/sashal/linux-stable.git]=sashal.linux-stable
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git]=stable.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git]=stable.linux-stable-rc
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/stable-queue.git]=stable.stable-queue
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git]=torvalds.linux
    OWNER_REPOS[https://gitlab.com/CKI-project/kernel-ark.git]=cki-project.kernel-ark
    OWNER_REPOS[https://host.com/gerrit/kernel-name3]=gerrit.kernel-name3
    OWNER_REPOS[https://host.com/gerrit/kernel-name4.git]=gerrit.kernel-name4

    for URL in "${!OWNER_REPOS[@]}"; do
        _check_equal "$(get_owner_repo "${URL}")" "${OWNER_REPOS[${URL}]}" "owner.repo" "Is the owner.repo correct for ${URL}"
    done
}
check_owner_repo

function check_get_auth_git_url() {
    export GITLAB_READ_REPO_TOKENS="$3"
    export READ_TOKEN=$4
    _check_equal "$(get_auth_git_url "$1")" "$2" "get_auth_git_url" "Is the auth URL correct for $1"
}
# Test no auth
check_get_auth_git_url "https://gitlab.example/example/repo" "https://gitlab.example/example/repo" "" "-"
# Test correct auth URL
check_get_auth_git_url "https://gitlab.example/example/repo" "https://oauth2:token@gitlab.example/example/repo" '{"gitlab.example": "READ_TOKEN"}' "token"
# Test auth for a different GitLab instance - thus no auth needed for our URL
check_get_auth_git_url "https://gitlab.different/example/repo" "https://gitlab.different/example/repo" '{"gitlab.example": "READ_TOKEN"}' "token"

# function() instead of function{} so that the stub functions are scoped
function check_git_cache_clone()
(
    declare aws_s3_download_params
    function git {
        /usr/bin/git "$@" > /dev/null 2>&1
        echo "git_params='$*'" >&2
    }
    function aws_s3_download {
        mkdir -p test-output/git-repo
        /usr/bin/git init test-output/git-repo > /dev/null 2>&1
        tar -C test-output/git-repo -cf - .
        echo "aws_s3_download_params='$*'" >&2
    }
    trap 'rm -rf test-output/' EXIT
    local URL="$1"
    local OWNER="$2"
    local FILE="$3"
    local git_params=''
    export GIT_CACHE_DIR="test-output/git-cache"
    eval "$(GIT_URL_OWNER=${OWNER} git_cache_clone "${URL}" tests/workdir --quiet 2>&1 >/dev/null)"
    read -ra git_params_array <<< "${git_params}"
    read -ra aws_s3_download_params_array <<< "${aws_s3_download_params}"

    _check_equal "${aws_s3_download_params_array[0]}" "BUCKET_GIT_CACHE" "bucket" "Does git_cache_clone pass the bucket correctly into aws_s3_download"
    _check_equal "${aws_s3_download_params_array[1]}" "${FILE}" "tarfile" "Does git_cache_clone pass the tar file name correctly into aws_s3_download"
    _check_equal "${git_params_array[-1]}" "--quiet" "param" "Does git_cache_clone pass the additional parameters to git clone"
    _check_equal "$(stat test-output/git-repo/.git/config > /dev/null && echo yes || echo no)" "yes" ".git/config exists" "Does the git repo get successfully cloned"
)
check_git_cache_clone https://host.com/a/b/repo.git "" b.repo.tar
check_git_cache_clone https://host.com/a/b/repo.git "kernel" kernel.repo.tar

# function() instead of function{} so that the stub functions are scoped
function check_git_clone()
(
    function git {
        :
    }
    _check_equal "$(git_clone "$1" /)" "$2" "output" "Are the credentials correctly hidden for $1"
)
check_git_clone https://oauth2:secret-token@gitlab.example/a/b/repo.git "[1;32mCloning https://REDACTED@gitlab.example/a/b/repo.git/ from upstream[0m"

# function() instead of function{} so that the stub functions are scoped
function check_cpu_count()
(
    local NPROC_RESULT="$1"
    local CGROUPS_V1_QUOTA_RESULT="$2"
    local CGROUPS_V2_QUOTA_RESULT="$3"
    local EXPECTED_CPUS_AVAILABLE="$4"
    local EXPECTED_MAKE_JOBS="$5"
    local EXPECTED_RPM_BUILD_NCPUS="$6"
    local MESSAGE="$7"
    function nproc {
        echo "${NPROC_RESULT}"
    }
    function get_cgroups_v1_quota {
        echo "${CGROUPS_V1_QUOTA_RESULT}"
    }
    function get_cgroups_v2_quota {
        echo "${CGROUPS_V2_QUOTA_RESULT}"
    }
    eval "$(get_cpu_count)"

    # shellcheck disable=SC2154
    _check_equal "${CPUS_AVAILABLE}" "${EXPECTED_CPUS_AVAILABLE}" cpu_count "Is the CPU count correct with ${MESSAGE}"
    # shellcheck disable=SC2154
    _check_equal "${MAKE_JOBS}" "${EXPECTED_MAKE_JOBS}" make_jobs "Is the make job count correct with ${MESSAGE}"
    # shellcheck disable=SC2154
    _check_equal "${RPM_BUILD_NCPUS}" "${EXPECTED_RPM_BUILD_NCPUS}" rpm_build_ncpus "Is the RPM build CPU count correct with ${MESSAGE}"
)
check_cpu_count 1000 800000              ""    8   12   12 "a cgroup v1 limit"
check_cpu_count 1000  50000              ""    1    1    1 "a cgroup v1 limit for less than 1 cpu"
check_cpu_count    1 800000              ""    1    1    1 "a cgroup v1 limit higher than the number of cpus"
check_cpu_count 1000     -1              "" 1000 1500 1500 "an unbounded cgroup v1 limit"
check_cpu_count 1000     "" "800000 100000"    8   12   12 "a cgroup v2 limit"
check_cpu_count 1000     ""  "50000 100000"    1    1    1 "a cgroup v2 limit for less than 1 cpu"
check_cpu_count    1     "" "800000 100000"    1    1    1 "a cgroup v2 limit higher than the number of cpus"
check_cpu_count 1000     ""    "max 100000" 1000 1500 1500 "an unbounded cgroup v2 limit"
check_cpu_count 1000     ""              "" 1000 1500 1500 "no cgroup v1 or v2 limit file"

function check_join_by() {
    local RETURNED d
    local -a ARRAY=(a b c)
    local -A DELIMITER_EXPECTED=(["&"]="a&b&c" [-]="a-b-c" )
    for d in "${!DELIMITER_EXPECTED[@]}"; do
        RETURNED=$(join_by "${d}" "${ARRAY[@]}")
        _check_equal "${RETURNED}" "${DELIMITER_EXPECTED[${d}]}" "check_by returned" "Is the join_by value correct for ${d} ${ARRAY[*]}"
    done
}
check_join_by

function check_join_by_multi() {
    local RETURNED d
    local -a ARRAY=(a b c)
    local -A DELIMITER_EXPECTED
    DELIMITER_EXPECTED[" --foo "]=" --foo a --foo b --foo c"
    DELIMITER_EXPECTED[" --bar="]=" --bar=a --bar=b --bar=c"
    for d in "${!DELIMITER_EXPECTED[@]}"; do
        RETURNED=$(join_by_multi "${d}" "${ARRAY[@]}")
        _check_equal "${RETURNED}" "${DELIMITER_EXPECTED[${d}]}" "check_by returned" "Is the join_by_multi value correct for ${d} ${ARRAY[*]}"
    done
}
check_join_by_multi

function check_create_array_from_string() {
    local string1="foo bar"
    local string2="  foo bar"
    local string3="foo bar "
    local string4="foo|bar|baz"
    local string5="foo README*" returned expected
    local string1_array string2_array string3_array string4_array string5_array
    for i in string{1..5}; do create_array_from_string "${i}"; done

    returned="(${string1_array[*]})"
    expected="(foo bar)"
    _check_equal "${returned}" "${expected}" "check_by returned" "Is the create_array_from_string value correct for '${string1}'"

    returned="(${string2_array[*]})"
    _check_equal "${returned}" "${expected}" "check_by returned" "Is the create_array_from_string value correct for '${string2}'"

    returned="(${string3_array[*]})"
    _check_equal "${returned}" "${expected}" "check_by returned" "Is the create_array_from_string value correct for '${string3}'"

    returned="(${string4_array[*]})"
    expected="(foo|bar|baz)"
    _check_equal "${returned}" "${expected}" "check_by returned" "Is the create_array_from_string value correct for '${string4}'"

    returned="(${string5_array[*]})"
    expected="(foo README*)"
    _check_equal "${returned}" "${expected}" "check_by returned" "Is the create_array_from_string value correct for '${string5}'"
}
check_create_array_from_string

function check_remove_from_array() {
    local result expected i
    local -a array=()
    local -A input_expected
    input_expected["a b c"]="a c"
    input_expected["b a c"]="a c"
    input_expected["a c b"]="a c"
    input_expected["a b b b c"]="a c"
    input_expected["a c d"]="a c d"
    input_expected["b b b"]=""
    for i in "${!input_expected[@]}"; do
        expected=${input_expected[${i}]}
        read -r -a array <<< "${i}"
        remove_from_array array b
        result="(${array[*]+"${array[*]}"})"
         _check_equal "${result}" "(${expected})" "check_by returned" "Is the remove_from_array correct for (${i}) 'b'"
    done
    # It handles elements with spaces.
    array=("a b" "c d")
    remove_from_array array "a b"
    _check_equal "${#array[@]}" "1" "check_by returned" "Is the remove_from_array size correct for ('a b' 'c d') 'a b'"
    result="('${array[0]}')"
    _check_equal "${result}" "('c d')" "check_by returned" "Is the remove_from_array correct for ('a b' 'c d') 'a b'"
     # It doesn't fail when the input array is empty.
    array=()
    remove_from_array array b
    result="(${array[*]+"${array[*]}"})"
    _check_equal "${result}" "()" "check_by returned" "Is the remove_from_array correct for () 'b'"
}
check_remove_from_array

function check_is_true() {
    declare -A IS_TRUE_MAPPING
    IS_TRUE_MAPPING["true"]=0
    IS_TRUE_MAPPING["True"]=0
    IS_TRUE_MAPPING["false"]=1
    IS_TRUE_MAPPING["False"]=1
    IS_TRUE_MAPPING["randomvalue"]=1

    for VALUE in "${!IS_TRUE_MAPPING[@]}"; do
        is_true "${VALUE}" && RETURNED=0 || RETURNED=$?
        _check_equal "${RETURNED}" "${IS_TRUE_MAPPING[${VALUE}]}" "is_true returned" "Is the is_true value correct for ${VALUE}"
    done
}
check_is_true

function check_git_url() {
    _check_equal "$(git_clean_url "$1")" "$2" "git_clean_url" "Is the git_clean_url $3"
}
check_git_url "https://foo.bar/test.git/" "https://foo.bar/test.git/" "passing through correct URLs"
check_git_url "https://foo.bar/test.git"  "https://foo.bar/test.git/" "appending a slash"
check_git_url "https://foo.bar/test"      "https://foo.bar/test.git/" "appending .git/"
check_git_url "https://foo.bar/test/"     "https://foo.bar/test.git/" "appending .git with an existing slash"
check_git_url "foo.bar/test.git/"         "https://foo.bar/test.git/" "prepending the protocol"

function loop_helper {
    STATE="$(cat /tmp/loop_state)"
    echo "$((STATE + 1))" > /tmp/loop_state
    # shellcheck disable=SC2086 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086]
    echo loop${STATE}-start >> /tmp/loop_log
    # shellcheck disable=SC2086 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086]
    if [ ${STATE} -lt $1 ]; then
        false
    fi
    # shellcheck disable=SC2086 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086]
    echo loop${STATE}-end >> /tmp/loop_log
}

function check_loop_successful {
    rm -f /tmp/loop_log /tmp/loop_state
    echo 1 > /tmp/loop_state
    loop_custom 5 0 loop_helper 3 &
    wait $! && s=0 || s=$?
    _check_equal "$(grep -c start /tmp/loop_log)" "3" "loops" "Is the command retried the correct number     of times"
    _check_equal "$(grep -c end /tmp/loop_log)" "1" "completions" "Is the command completing exactly one     time"
    _check_equal "${s}" "0" "exit code" "Does the loop command exit correctly"
}
check_loop_successful

function check_loop_failed {
    rm -f /tmp/loop_log /tmp/loop_state
    echo 1 > /tmp/loop_state
    loop_custom 5 0 loop_helper 10 &
    wait $! && s=0 || s=$?
    _check_equal "$(grep -c start /tmp/loop_log)" "5" "loops" "Is the command retried the maximum number     of times"
    _check_equal "$(grep -c end /tmp/loop_log)" "0" "completions" "Is the command never completing"
    _check_equal "${s}" "1" "exit code" "Does the loop command fail correctly"
}
check_loop_failed

function check_dryrun_by_upt() {
    local TEST_OUTPUT_FILE=/tmp/check_dryrun_by_upt.testfile
    local ARG_INPUT=/tmp/input.xml
    local ARG_OUTPUT=/tmp/output.yaml

    dryrun_by_upt "echo" "${ARG_INPUT}" "${ARG_OUTPUT}" > "${TEST_OUTPUT_FILE}" || true

    local CAPTURE=0
    grep "\-m upt legacy convert \-i ${ARG_INPUT} \-r ${ARG_OUTPUT}" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to UPT ok"

    CAPTURE=0
    grep "\-m restraint_wrap --help" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to restraint runner ok"
}
check_dryrun_by_upt

function check_provision_and_test_by_upt() {
    local TEST_OUTPUT_FILE=/tmp/testfile

    local ARG_PY3=echo
    local ARG_RC=/tmp/rc
    local ARG_UPT_INPUT=/tmp/input.xml
    local ARG_UPT_OUTPUT_YAML=/tmp/output.yaml
    local ARG_EXCLUDE_FILE=/tmp/hosts
    local ARG_UPT_OUTPUT_DIR=/tmp

    touch "${ARG_UPT_OUTPUT_YAML}"

    provision_and_test_by_upt "${ARG_PY3}" "${ARG_RC}" "${ARG_EXCLUDE_FILE}" "${ARG_UPT_INPUT}" "${ARG_UPT_OUTPUT_YAML}" "${ARG_UPT_OUTPUT_DIR}" "beaker" "--upload" > "${TEST_OUTPUT_FILE}" "high" || true

    local CAPTURE=0
    grep "\-m upt \-\-time\-cap 4200 \-\-rc ${ARG_RC} \-e ${ARG_EXCLUDE_FILE} \-\-max-aborted-count 6 legacy convert \-i ${ARG_UPT_INPUT} \-r ${ARG_UPT_OUTPUT_YAML} \-p beaker \-\-priority high" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to upt legacy convert ok"
    CAPTURE=0
    grep "\-m upt \-\-time\-cap 4200 \-\-rc ${ARG_RC} \-e ${ARG_EXCLUDE_FILE} \-\-max-aborted-count 6 provision --pipeline \-\-reruns 1 \-\-upload \-\-dump \-o ${ARG_UPT_OUTPUT_DIR} \-r ${ARG_UPT_OUTPUT_YAML}" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to upt provision ok"
}
check_provision_and_test_by_upt

# function() instead of function{} so that the helper functions are scoped
function check_validate_merge_request_commit()
(
    function git_commit() {
        git checkout "$1" || true
        echo "$2" > "$2"
        git add "$2"
        git commit -m "$2"
    }
    function git_merge() {
        git checkout "$2"
        git branch "$1"
        git checkout "$1"
        git merge -m "$3" "$3"
    }
    function git_init() {
        git init .
        git symbolic-ref HEAD refs/heads/main
        git config user.name ci
        git config user.email ci@gitlab.com
        git_commit main initial
        git branch feature
        git branch other-feature
        git_commit main "first main"
        git_commit main "second main"
        git_commit feature "first feature"
        git_commit feature "second feature"
        git_merge m1-f1 main^ feature^
        git_merge m1-f2 main^ feature
        git_merge m2-f1 main feature^
        git_merge m2-f2 main feature
    }
    function check_validation() {
        declare -x valid
        git branch -f mr-target main
        git branch -f mr-feature feature
        git branch -f mr-merge "$1"
        validate_merge_request_commit > /dev/null && valid=yes || valid=no
        _check_equal "${valid}" "$2" "valid MR commit" "$3"
    }

    # shellcheck disable=SC2034  # mr_id used by validate_merge_request_commit
    local mr_id=123
    local GIT_REPO=/tmp/tests-functions-mr-repo.git
    rm -fr "${GIT_REPO}"
    mkdir -p "${GIT_REPO}"
    pushd "${GIT_REPO}" > /dev/null
    git_init > /dev/null 2>&1

    check_validation main  no  "Is a missing MR merge commit rejected"
    check_validation m1-f1 no  "Is an MR merge commit based on outdated target and feature branches rejected"
    check_validation m1-f2 no  "Is an MR merge commit based on an outdated target branch rejected"
    check_validation m2-f1 no  "Is an MR merge commit based on an outdated feature branch rejected"
    check_validation m2-f2 yes "Is a current MR merge commit accepted"

    popd
)
check_validate_merge_request_commit

function check_kcidb_skip_all_tests() {
    local CI_PROJECT_DIR=/tmp/check_set_as_skip.testfile
    local KCIDB_DUMPFILE_NAME=kcidb_all.json

    mkdir -p "${CI_PROJECT_DIR}"

    # kcidb is not available so let's just mock the output
    cat <<EOF > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
{
  "version": {"major": 4, "minor": 0},
  "tests": [
     {"id": "redhat:1", "origin": "redhat", "build_id": "build:1"},
     {"id": "redhat:2", "origin": "redhat", "build_id": "build:1"},
     {"id": "redhat:3", "origin": "redhat", "build_id": "build:1"}
  ]
}
EOF

    # Check kcidb calls (again, not available)
    local call_count=0
    local call_ids=()
    function kcidb() {
        _check_equal "${1} *id* ${3} ${4} ${5}" "test *id* set status SKIP" "call_args" "kcidb call args"
        call_count=$(( call_count + 1 ))
        call_ids+=("${2}")
    }

    kcidb_skip_all_tests

    _check_equal "${call_count}" "3" "call_count" "kcidb is called once per test"
    _check_equal "${call_ids[*]}" "redhat:1 redhat:2 redhat:3" "call_ids" "kcidb is called once per test"

}
check_kcidb_skip_all_tests

function check_kcidb_skip_missing_tests() {
    local CI_PROJECT_DIR=/tmp/check_set_as_skip.testfile
    local KCIDB_DUMPFILE_NAME=kcidb_all.json

    mkdir -p "${CI_PROJECT_DIR}"

    # kcidb is not available so let's just mock the output
    cat <<EOF > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
{
  "version": {"major": 4, "minor": 0},
  "tests": [
     {"id": "redhat:1", "origin": "redhat", "build_id": "build:1", "status": "PASS"},
     {"id": "redhat:2", "origin": "redhat", "build_id": "build:1"},
     {"id": "redhat:3", "origin": "redhat", "build_id": "build:1"}
  ]
}
EOF

    # Check kcidb calls (again, not available)
    local calls=()
    function kcidb() {
        calls+=("${1} ${2} ${3} ${4} ${5}")
    }

    kcidb_skip_missing_tests

    _check_equal "${calls[0]}" "test redhat:2 set status SKIP" "status" "Is status set to SKIP"
    _check_equal "${calls[1]}" "test redhat:2 set-bool misc/forced_skip_status true" "misc/forced_skip_status" "Is forced_skip_status set to true"
    _check_equal "${calls[2]}" "test redhat:3 set status SKIP" "status" "Is status set to SKIP"
    _check_equal "${calls[3]}" "test redhat:3 set-bool misc/forced_skip_status true" "misc/forced_skip_status" "Is forced_skip_status set to true"
}
check_kcidb_skip_missing_tests

function check_build_failure_extraction() {
    local CI_PROJECT_DIR=/tmp/build-logs/
    local BUILDLOG_PATH=build.log
    local BUILD_FAILURE_LOG_PATH=failure.log
    local KCIDB_DUMPFILE_NAME=kcidb_all.json
    export KCIDB_BUILD_ID="redhat:123"

    mkdir -p "${CI_PROJECT_DIR}"
    cat <<EOF > "${CI_PROJECT_DIR}${KCIDB_DUMPFILE_NAME}"
{
  "version": {"major": 4, "minor": 0},
  "builds": [{"id": "redhat:123", "origin": "redhat"}]
}
EOF
    cat <<EOF > "${CI_PROJECT_DIR}${BUILDLOG_PATH}"
00:00:28 Warning: Kernel ABI header at 'tools/arch/s390/include/uapi/asm/kvm.h' differs from latest version at 'arch/s390/include/uapi/asm/kvm.h'
00:00:28 diff -u tools/arch/s390/include/uapi/asm/kvm.h arch/s390/include/uapi/asm/kvm.h
00:00:28 Warning: Kernel ABI header at 'tools/perf/trace/beauty/include/linux/socket.h' differs from latest version at 'include/linux/socket.h'
00:00:28 diff -u tools/perf/trace/beauty/include/linux/socket.h include/linux/socket.h
00:00:28 /usr/bin/make FIXDEP=1 -f Makefile.perf
00:00:35 Makefile.config:450: *** No gnu/libc-version.h found, please install glibc-dev[el].  Stop.
00:00:35 make[1]: *** [Makefile.perf:240: sub-make] Error 2
00:00:35 make: *** [Makefile:70: all] Error 2
00:00:35 error: Bad exit status from /var/tmp/rpm-tmp.sexpvC (%build)
00:00:35
00:00:35 RPM build errors:
this line should not be extracted ok?
00:00:12   CC      mm/early_ioremap.o
00:00:12   CC      drivers/pinctrl/freescale/pinctrl-imx8mp.o
00:00:12   CC [M]  drivers/phy/marvell/phy-mvebu-cp110-comphy.o
00:00:12   CC      net/core/netclassid_cgroup.o
00:00:12   CC      mm/cma.o
00:00:12 arch/arm64/kernel/kexec_image.c:136:23: error: ‘kexec_kernel_verify_pe_sig’ undeclared here (not in a function)
00:00:12   136 |         .verify_sig = kexec_kernel_verify_pe_sig,
00:00:12       |                       ^~~~~~~~~~~~~~~~~~~~~~~~~~
00:00:12 make[4]: *** [scripts/Makefile.build:289: arch/arm64/kernel/kexec_image.o] Error 1
00:00:12   CC      drivers/pinctrl/freescale/pinctrl-imx8mq.o
00:00:12 make[3]: *** [scripts/Makefile.build:551: arch/arm64/kernel] Error 2
00:13:17 + /tmp/rpmbuild/SOURCES/check-kabi -k /tmp/rpmbuild/BUILDROOT/kernel-4.18.0-417.el8.mr2858_220811_1843.g7a68.aarch64/Module.kabi -s Module.symvers
00:13:17 *** ERROR - ABI BREAKAGE WAS DETECTED ***
00:13:17
00:13:17 The following symbols have been changed (this will cause an ABI breakage):
00:10:18 Error: Mismatches found in configuration files for arm lpae-fedora
00:10:18 Found CONFIG_CRYPTO_BLAKE2S_ARM=y after generation, had CONFIG_CRYPTO_BLAKE2S_ARM=m in Source tree
00:10:18 Error: Mismatches found in configuration files for arm lpae-kgcov-fedora
00:10:18 Found CONFIG_CRYPTO_BLAKE2S_ARM=y after generation, had CONFIG_CRYPTO_BLAKE2S_ARM=m in Source tree
00:10:18 Error: Mismatches found in configuration files for x86_64 debug-fedora
EOF

    print_and_save_build_failure "${KCIDB_BUILD_ID}" > /dev/null
    # The number of lines is still 32 because of the match divider "--" line
    _check_equal "$(wc -l < "${CI_PROJECT_DIR}${BUILD_FAILURE_LOG_PATH}")" "32" "Number of extracted lines" "Is the number of extracted lines correct"
    grep -q "this line should not be extracted ok?" "${CI_PROJECT_DIR}${BUILD_FAILURE_LOG_PATH}" && correct=no || correct=yes
    _check_equal "${correct}" "yes" "line missing" "Is a non-matching line outside of context missing"
}
check_build_failure_extraction

function check_merge_failure_extraction() {
    local CI_PROJECT_DIR=/tmp/build-logs/
    local MERGELOG_PATH=merge.log
    local MERGE_FAILURE_LOG_PATH=failure.log
    local KCIDB_DUMPFILE_NAME=kcidb_all.json
    export KCIDB_CHECKOUT_ID="redhat:123"

    mkdir -p "${CI_PROJECT_DIR}"
    cat <<EOF > "${CI_PROJECT_DIR}${KCIDB_DUMPFILE_NAME}"
{
  "version": {"major": 4, "minor": 0},
  "checkouts": [{"id": "redhat:123", "origin": "redhat"}]
}
EOF
    cat <<EOF > "${CI_PROJECT_DIR}${MERGELOG_PATH}"
hint: You can replace "git config" with "git config --global" to set a default
hint: preference for all repositories. You can also pass --rebase, --no-rebase,
hint: or --ff-only on the command line to override the configured default per
hint: invocation.
Auto-merging redhat/kernel.spec.template
Auto-merging redhat/git/files
Auto-merging net/sched/sch_generic.c
CONFLICT (content): Merge conflict in net/sched/sch_generic.c
Auto-merging net/sched/sch_api.c
CONFLICT (content): Merge conflict in net/sched/sch_api.c
Auto-merging net/core/gen_stats.c
CONFLICT (content): Merge conflict in net/core/gen_stats.c
Auto-merging net/core/gen_estimator.c
CONFLICT (content): Merge conflict in net/core/gen_estimator.c
Auto-merging net/core/dev.c
Removing kernel/printk/printk_safe.c
Removing kernel/printk/internal.h
error: patch failed: kernel/sched/fair.c:3298
error: kernel/sched/fair.c: patch does not apply
hint: Use 'git am --show-current-patch=diff' to see the failed patch
Applying: sched/fair: Ensure that the CFS parent is added after unthrottling
Patch failed at 0001 sched/fair: Ensure that the CFS parent is added after unthrottling
When you have resolved this problem, run "git am --continue".
If you prefer to skip this patch, run "git am --skip" instead.
To restore the original branch and stop patching, run "git am --abort".
EOF

    save_merge_failure
    _check_equal "$(wc -l < "${CI_PROJECT_DIR}${MERGE_FAILURE_LOG_PATH}")" "22" "Number of extracted lines" "Is the number of extracted lines correct"
}
check_merge_failure_extraction

_failed_check
