#!/bin/bash

eval "$(shellspec - -c) exit 1"

Describe 'base kernel configuration download'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    curl() {
        echo "${3}"
    }

    aws_s3_url() {
        echo "${2}"
    }

    Describe 'is correct kernel config file downloaded'
        ARCH_CONFIG='arch'
        It 'downloads Fedora config if variable is empty'
            When call get_base_kernel_config ""
            The output should equal "${ARCH_CONFIG}.config"
        End

        It 'downloads specified config if variable is set'
            example_url="this_is_url"
            When call get_base_kernel_config "${example_url}"
            The output should equal "${example_url}"
        End
    End
End
