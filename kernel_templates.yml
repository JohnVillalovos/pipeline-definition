---

# Default kernel repo workflow
.workflow:
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_PROJECT_ID != $CI_MERGE_REQUEST_PROJECT_ID'
      variables:
        REQUESTED_PIPELINE_TYPE: 'insufficient-permissions'
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.rhel/ || $CI_PROJECT_PATH =~ /^redhat.rhel/'
      variables:
        REQUESTED_PIPELINE_TYPE: 'internal'
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.centos-stream/ || $CI_PROJECT_PATH =~ /^redhat.centos-stream/ ||
           $CI_MERGE_REQUEST_PROJECT_PATH =~ /^CentOS/ || $CI_PROJECT_PATH =~ /^CentOS/'
      variables:
        REQUESTED_PIPELINE_TYPE: '/^(trusted|centos-rhel)$/'
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.prdsc/ || $CI_PROJECT_PATH =~ /^redhat.prdsc/'
      variables:
        REQUESTED_PIPELINE_TYPE: 'scratch'

# explicitly fail pipelines running in a fork instead of the parent repo
# most likely a click on https://red.ht/GitLabSSO is needed for RH employees
insufficient-permissions:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/insufficient-permissions
    strategy: depend
  rules:
    - if: '$REQUESTED_PIPELINE_TYPE == "insufficient-permissions"'

# Merge request pipeline
.merge_request:
  variables:
    title: ${CI_COMMIT_TITLE}
    commit_hash: ${CI_COMMIT_SHA}
    git_url: ${CI_MERGE_REQUEST_PROJECT_URL}.git
    branch: ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}
    mr_id: ${CI_MERGE_REQUEST_IID}
    mr_url: ${CI_MERGE_REQUEST_PROJECT_URL}/-/merge_requests/${CI_MERGE_REQUEST_IID}
    mr_project_id: ${CI_MERGE_REQUEST_PROJECT_ID}
    mr_source_branch_hash: ${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA}  # only set on (non-draft) merged result pipelines
    trigger_job_name: ${CI_JOB_NAME}
  rules:
    - if: '$PIPELINE_TYPE =~ $REQUESTED_PIPELINE_TYPE && $CI_MERGE_REQUEST_ID && (
             ($RUN_ONLY_FOR_RT =~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /-rt$/) ||
             ($RUN_ONLY_FOR_AUTOMOTIVE =~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /-automotive$/) ||
             (
               ($RUN_ONLY_FOR_RT !~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME !~ /-rt$/) &&
               ($RUN_ONLY_FOR_AUTOMOTIVE !~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME !~ /-automotive$/)
             )
          )'

# Baseline pipeline
.baseline:
  variables:
    title: ${CI_COMMIT_TITLE}
    commit_hash: ${CI_COMMIT_SHA}
    git_url: ${CI_PROJECT_URL}.git
    branch: ${CI_COMMIT_BRANCH}
    test_set: 'kt1'
    scratch: 'false'
    trigger_job_name: ${CI_JOB_NAME}
  rules:
    - if: '$PIPELINE_TYPE =~ $REQUESTED_PIPELINE_TYPE && $CI_COMMIT_BRANCH && (
             ($RUN_ONLY_FOR_RT =~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH =~ /-rt$/) ||
             ($RUN_ONLY_FOR_AUTOMOTIVE =~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH =~ /-automotive$/) ||
             (
               ($RUN_ONLY_FOR_RT !~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH !~ /-rt$/) &&
               ($RUN_ONLY_FOR_AUTOMOTIVE !~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH !~ /-automotive$/)
             )
          )'

# Trigger internal child pipeline
.internal:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-internal-contributors
    strategy: depend
  variables:
    PIPELINE_TYPE: 'internal'
    kernel_type: internal  # needs to match the pipeline project in trigger/project!
    publish_elsewhere: 'true'  # needs to match the pipeline project in trigger/project!
    # use fixed git cache owner as group of kernel repositories might differ from 'kernel'
    git_url_cache_owner: kernel
    merge_tree_cache_owner: kernel

.centos_stream_rhel_internal:
  extends: .internal
  variables:
    PIPELINE_TYPE: 'centos-rhel'

# Trigger trusted child pipeline
.trusted:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-trusted-contributors
    strategy: depend
  variables:
    PIPELINE_TYPE: 'trusted'
    kernel_type: upstream  # needs to match the pipeline project in trigger/project!

# Trigger scratch child pipeline
.scratch:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-scratch-pipelines/scratch-pipelines
    strategy: depend
  variables:
    PIPELINE_TYPE: 'scratch'
    kernel_type: internal  # needs to match the pipeline project in trigger/project!
    publish_elsewhere: 'true'  # needs to match the pipeline project in trigger/project!
    git_url_cache_owner: kernel
    merge_tree_cache_owner: kernel
    skip_results: 'true'
    test_priority: 'urgent'

# Only build and publish, exclude setup, test and results
.only_build_and_publish:
  variables:
    skip_setup: 'true'  # No need to send pre-test messages
    skip_test: 'true'  # Skip testing, only need to look out for merge/build problems
    skip_results: 'true'  # Any issues should be caused by conflicts

# Send test notifications
.with_notifications:
  variables:
    send_ready_for_test_pre: 'true'
    send_ready_for_test_post: 'true'

# Common RHEL settings
.rhel_common:
  variables:
    artifacts_mode: s3
    test_debug: 'true'

# Common realtime_check settings
.realtime_check_common:
  extends: [.rhel_common]
  allow_failure: true  # Don't block MRs!
  variables:
    package_name: kernel-rt
    merge_tree: ${CI_MERGE_REQUEST_PROJECT_URL}.git
    merge_branch: ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}-rt
    architectures: 'x86_64'  # Supported on x86_64 only

# Common full RT pipeline settings
.realtime_pipeline_common:
  extends: .rhel_common
  variables:
    package_name: kernel-rt
    architectures: 'x86_64'  # Supported on x86_64 only

# Baseline realtime checks: Full runs
.realtime_check_baseline:
  extends: .realtime_pipeline_common
  variables:
    merge_tree: ${CI_PROJECT_URL}.git
    merge_branch: ${CI_COMMIT_BRANCH}-rt
    test_set: rt

# Common full automotive pipeline settings
.automotive_pipeline_common:
  extends: .rhel_common
  variables:
    package_name: kernel-automotive
    architectures: 'x86_64 aarch64'  # Supported on x86_64/arm only
    test_runner: aws
    test_set: automotive
    AWS_UPT_IMAGE_OWNER: '587138297281'
    AWS_UPT_LAUNCH_TEMPLATE_NAME: 'arr-cki.prod.lt.upt-automotive'

# Automotive check configs
.automotive_check_common:
  extends: .automotive_pipeline_common
  allow_failure: true  # Don't block MRs!
  variables:
    merge_tree: ${CI_MERGE_REQUEST_PROJECT_URL}.git
    merge_branch: ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}-automotive

# Coverage variables
.coverage:
  variables:
    coverage: 'true'
    artifacts_mode: s3
    test_debug: 'false'  # Override from .rhel_common because it's not supported by specfile
